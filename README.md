# Spring Boot + GraphQL + MySQL example
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-graphql-mysql.git`.
2. Go inside the folder: `cd springboot-graphql-mysql`.
3. Run the application: `mvn clean spring-boot:run`

### Run & Check result
Tables will be automatically generated in Database.

If you check MySQL database, you can see them:
```sql
MariaDB [graphQL]> describe author;
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| id    | bigint(20)   | NO   | PRI | NULL    |       |
| age   | int(11)      | YES  |     | NULL    |       |
| name  | varchar(255) | NO   |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+
3 rows in set (0.005 sec)

MariaDB [graphQL]> describe tutorial;
+-------------+--------------+------+-----+---------+-------+
| Field       | Type         | Null | Key | Default | Extra |
+-------------+--------------+------+-----+---------+-------+
| id          | bigint(20)   | NO   | PRI | NULL    |       |
| description | varchar(255) | YES  |     | NULL    |       |
| title       | varchar(255) | NO   |     | NULL    |       |
| author_id   | bigint(20)   | NO   | MUL | NULL    |       |
+-------------+--------------+------+-----+---------+-------+
4 rows in set (0.003 sec)
```

### Screen shot

Create an Author

![Create an Author](img/create_author.png "Create an Author")

Create a Tutorial

![Create a Tutorial](img/create_tutorial.png "Create a Tutorial")

Find All tutorials

![Find All tutorials](img/find.png "Find All tutorials")

List All Authors

![List All Authors](img/list_author.png "List All Authors")

Update Tutorial

![Update Tutorial](img/update_tutorial.png "Update Tutorial")

Delete Tutorial

![Delete Tutorial](img/delete_tutorial.png "Delete Tutorial")

Count Tutorial

![Count Tutorial](img/count_tutorial.png "Count Tutorial")

For checking result, you can use Postman to make HTTP POST request to http://localhost:8080/apis/graphql.

If you check MySQL database, it will have 2 tables: author & tutorial.

The content inside these tables looks like:
```sql
MariaDB [graphQL]> select * from author;
+----+------+----------------+
| id | age  | name           |
+----+------+----------------+
|  1 |   35 | Uzumaki Naruto |
|  2 |   37 | Uchiha Sasuke  |
+----+------+----------------+
2 rows in set (0.002 sec)

MariaDB [graphQL]> select * from tutorial;
+----+-----------------------+-------------+-----------+
| id | description           | title       | author_id |
+----+-----------------------+-------------+-----------+
|  3 | updated Desc Tut#3    | Tutorial #1 |         1 |
|  5 | Description for Tut#3 | Tutorial #3 |         1 |
+----+-----------------------+-------------+-----------+
2 rows in set (0.001 sec)

```