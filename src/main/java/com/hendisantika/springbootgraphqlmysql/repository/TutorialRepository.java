package com.hendisantika.springbootgraphqlmysql.repository;

import com.hendisantika.springbootgraphqlmysql.model.Tutorial;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-graphql-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/04/20
 * Time: 07.45
 */
public interface TutorialRepository extends JpaRepository<Tutorial, Long> {
}
