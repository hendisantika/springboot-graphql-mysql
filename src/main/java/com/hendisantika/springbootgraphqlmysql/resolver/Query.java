package com.hendisantika.springbootgraphqlmysql.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.hendisantika.springbootgraphqlmysql.model.Author;
import com.hendisantika.springbootgraphqlmysql.model.Tutorial;
import com.hendisantika.springbootgraphqlmysql.repository.AuthorRepository;
import com.hendisantika.springbootgraphqlmysql.repository.TutorialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-graphql-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/04/20
 * Time: 07.49
 */
@Component
public class Query implements GraphQLQueryResolver {
    private final AuthorRepository authorRepository;
    private final TutorialRepository tutorialRepository;

    @Autowired
    public Query(AuthorRepository authorRepository, TutorialRepository tutorialRepository) {
        this.authorRepository = authorRepository;
        this.tutorialRepository = tutorialRepository;
    }

    public Iterable<Author> findAllAuthors() {
        return authorRepository.findAll();
    }

    public Iterable<Tutorial> findAllTutorials() {
        return tutorialRepository.findAll();
    }

    public long countAuthors() {
        return authorRepository.count();
    }

    public long countTutorials() {
        return tutorialRepository.count();
    }
}
