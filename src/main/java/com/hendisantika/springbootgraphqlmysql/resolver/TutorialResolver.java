package com.hendisantika.springbootgraphqlmysql.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.springbootgraphqlmysql.model.Author;
import com.hendisantika.springbootgraphqlmysql.model.Tutorial;
import com.hendisantika.springbootgraphqlmysql.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-graphql-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/04/20
 * Time: 07.50
 */
@Component
public class TutorialResolver implements GraphQLResolver<Tutorial> {
    @Autowired
    private final AuthorRepository authorRepository;

    public TutorialResolver(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author getAuthor(Tutorial tutorial) {
        return authorRepository.findById(tutorial.getAuthor().getId()).orElseThrow(null);
    }
}
