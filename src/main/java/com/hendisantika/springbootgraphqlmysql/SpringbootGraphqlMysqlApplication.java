package com.hendisantika.springbootgraphqlmysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGraphqlMysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootGraphqlMysqlApplication.class, args);
    }

}
